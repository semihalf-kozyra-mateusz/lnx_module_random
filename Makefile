ARCH:=arm64
CROSS_COMPILE:=aarch64-linux-gnu-
KDIR:=/home/mkoz/linux
PWD:=$(PWD)
obj-m+=my_rand.o

export
all : 
	make -C $(KDIR) M=$(PWD) modules 

clean   : 
	make -C $(KDIR) M=$(PWD) clean 


